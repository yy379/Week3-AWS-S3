"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3AppStack = void 0;
const cdk = require("aws-cdk-lib");
// import * as sqs from 'aws-cdk-lib/aws-sqs';
class S3AppStack extends cdk.Stack {
    constructor(scope, id, props) {
        super(scope, id, props);
        // Add bucket properties like versioning and encryption
        const bucket = new cdk.aws_s3.Bucket(this, 'SecondBucket', {
            versioned: true,
            encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
        });
    }
}
exports.S3AppStack = S3AppStack;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiczMtYXBwLXN0YWNrLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiczMtYXBwLXN0YWNrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7OztBQUFBLG1DQUFtQztBQUVuQyw4Q0FBOEM7QUFFOUMsTUFBYSxVQUFXLFNBQVEsR0FBRyxDQUFDLEtBQUs7SUFDdkMsWUFBWSxLQUFnQixFQUFFLEVBQVUsRUFBRSxLQUFzQjtRQUM5RCxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUV4Qix1REFBdUQ7UUFDdkQsTUFBTSxNQUFNLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsY0FBYyxFQUFFO1lBQ3pELFNBQVMsRUFBRSxJQUFJO1lBQ2YsVUFBVSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsVUFBVTtTQUVuRCxDQUFDLENBQUM7SUFFTCxDQUFDO0NBQ0Y7QUFaRCxnQ0FZQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIGNkayBmcm9tICdhd3MtY2RrLWxpYic7XG5pbXBvcnQgeyBDb25zdHJ1Y3QgfSBmcm9tICdjb25zdHJ1Y3RzJztcbi8vIGltcG9ydCAqIGFzIHNxcyBmcm9tICdhd3MtY2RrLWxpYi9hd3Mtc3FzJztcblxuZXhwb3J0IGNsYXNzIFMzQXBwU3RhY2sgZXh0ZW5kcyBjZGsuU3RhY2sge1xuICBjb25zdHJ1Y3RvcihzY29wZTogQ29uc3RydWN0LCBpZDogc3RyaW5nLCBwcm9wcz86IGNkay5TdGFja1Byb3BzKSB7XG4gICAgc3VwZXIoc2NvcGUsIGlkLCBwcm9wcyk7XG5cbiAgICAvLyBBZGQgYnVja2V0IHByb3BlcnRpZXMgbGlrZSB2ZXJzaW9uaW5nIGFuZCBlbmNyeXB0aW9uXG4gICAgY29uc3QgYnVja2V0ID0gbmV3IGNkay5hd3NfczMuQnVja2V0KHRoaXMsICdTZWNvbmRCdWNrZXQnLCB7XG4gICAgICB2ZXJzaW9uZWQ6IHRydWUsXG4gICAgICBlbmNyeXB0aW9uOiBjZGsuYXdzX3MzLkJ1Y2tldEVuY3J5cHRpb24uUzNfTUFOQUdFRCxcblxuICAgIH0pO1xuXG4gIH1cbn1cbiJdfQ==