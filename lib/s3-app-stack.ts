import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class S3AppStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Add bucket properties like versioning and encryption
    const bucket = new cdk.aws_s3.Bucket(this, 'SecondBucket', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,

    });

  }
}
