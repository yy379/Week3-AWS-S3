#  Create an S3 Bucket using CDK with AWS CodeWhisperer.

This is a project for CDK development with TypeScript in IDS721.

## Requirements
- Create S3 bucket using AWS CDK
- Use CodeWhisperer to generate CDK code
- Add bucket properties like versioning and encryption

## Dependencies
- Node.js
- AWS CLI
- AWS CDK
- npm

## Steps    
1. Install dependencies
2. Create a new project using `cdk init app --language typescript`
3. Using CodeWhisperer, generate the CDK code for the S3 bucket
![CodeWhisperer](./screenshots/CodeWhisperer.png)
4. Build the project using `npm run build`
5. Bootstrap the AWS environment using `cdk bootstrap`
![Bootstrap](./screenshots/bootstrap.png)
6. Deploy the stack using `cdk deploy`
![Deploy](./screenshots/deploy.png)
7. Check the AWS console to verify the S3 bucket
![S3 Bucket](./screenshots/aws.png)

## Useful commands

* `npm run build`   compile typescript to js
* `npm run watch`   watch for changes and compile
* `npm run test`    perform the jest unit tests
* `npx cdk deploy`  deploy this stack to your default AWS account/region
* `npx cdk diff`    compare deployed stack with current state
* `npx cdk synth`   emits the synthesized CloudFormation template

## References
- [AWS CDK Developer Guide](https://docs.aws.amazon.com/cdk/v2/guide/hello_world.html)